using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField]
    private float openAngle = -90f;
    [SerializeField]
    private float closeAngle = 0f;
    [SerializeField]
    private float speed = 2f; 


    private Quaternion openRotation; 
    private Quaternion closeRotation; 
    private bool isOpen = false; 

    private void Start()
    {
        openRotation = Quaternion.Euler(0, openAngle, 0);
        closeRotation = Quaternion.Euler(0, closeAngle, 0);
    }

    public void ToggleDoor()
    {
        
        isOpen = !isOpen;

        Quaternion targetRotation = isOpen ? openRotation : closeRotation;

        StartCoroutine(AnimateDoor(targetRotation));
    }

    private IEnumerator AnimateDoor(Quaternion targetRotation)
    {
        float t = 0f;
        Quaternion initialRotation = transform.rotation;

        while (t < 1f)
        {
            t += Time.deltaTime * speed;
            transform.rotation = Quaternion.Lerp(initialRotation, targetRotation, t);
            yield return null;
        }
    }
}
